
export interface Post {
    id: number;
    title: string;
    image: string;
    short_description: string;
    description: string;
    autor: string;
    created_at: Date;
}

export interface CreatePost {
    title: string;
    image: string;
    short_description: string;
    description: string;
    autor: string;
}


export interface PostList {
    id: number;
    title: string;
    image: string;
    short_description: string;
    autor: string;
    created_at: string;
}