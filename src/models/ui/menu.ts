export interface Menu {
    url?: string;
    name: string;
    icon: string;
    actions?: (T) => typeof T
}