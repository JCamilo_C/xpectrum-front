export interface ResponseModel<T> {
    Response: boolean;
    Message: string;
    Result: T;
}

export interface Paginate<T> {
    pages: number;
    items: T;
    total: number;
}