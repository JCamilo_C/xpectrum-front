import { User } from "./user.model";

export interface Authentication {
    token: string;
    user: User;
}