import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthComponent } from './views/auth/auth.component';
import { BlogComponent } from './views/blog/blog.component';
import { AuthGuardGuard } from './guards/auth-guard.guard';

const routes: Routes = [
  { 
    path: '', 
    component: BlogComponent, 
    canActivate: [AuthGuardGuard],
    loadChildren: () => import('./views/blog/blog.module').then(m => m.BlogModule)
  },
  { 
    path: 'Auth', 
    component: AuthComponent, 
    loadChildren: () => import('./views/auth/auth.module').then(m => m.AuthModule)
  },
  { path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
