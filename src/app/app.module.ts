import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
 
import { ToastrModule } from 'ngx-toastr';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthComponent } from './views/auth/auth.component';
import { StoreModule } from '@ngrx/store';
import { appReducers, metaReducers } from '../redux/app.reducers';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment.prod';
import { TokenService } from './services/interceptors/token.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { EffectsModule } from '@ngrx/effects';
import { AuthEffects } from '../redux/effects/auth.effects';
import { ErrorTokenService } from './services/interceptors/error-token.service';
import { BlogComponent } from './views/blog/blog.component';
import { UiEffects } from '../redux/effects/ui.effects';
import { SharedModule } from './shared/shared.module';
import { registerLocaleData, CommonModule } from '@angular/common';

import localePy from '@angular/common/locales/es-PY';

registerLocaleData(localePy, 'es')

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    BlogComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(), // ToastrModule added
    StoreModule.forRoot(appReducers, {metaReducers}),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production, // Restrict extension to log-only mode
    }),
    EffectsModule.forRoot([AuthEffects, UiEffects]),
    SharedModule

  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: TokenService, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorTokenService, multi: true },
    { provide: LOCALE_ID, useValue: 'es' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
