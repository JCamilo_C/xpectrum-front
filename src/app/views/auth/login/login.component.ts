import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { AppState } from 'src/redux/app.reducers';
import * as authActions from '../../../../redux/actions/auth.actions';
import * as selectors from '../../../../redux/selectors';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loading$: Observable<boolean> = of(false);

  constructor(private store: Store<AppState>) { 
    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.email, Validators.required]),
      password: new FormControl('', [Validators.required]),
    });

    this.loading$ = this.store.select(selectors.GetAuthLoading);
  }

  ngOnInit(): void {
  }

  submit() {
    const payload: {email:string, password: string} = this.loginForm.value;

    this.store.dispatch(authActions.LoadingLoginAction(payload));
  }

}
