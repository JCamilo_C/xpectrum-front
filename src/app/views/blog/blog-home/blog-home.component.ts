import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { PostList } from '../../../../models/blog/post';
import { AppStateBlog } from '../blog.module';
import * as selectors from '../../../../redux/selectors';
import * as actions from '../../../../redux/actions';

@Component({
  selector: 'app-blog-home',
  templateUrl: './blog-home.component.html',
  styleUrls: ['./blog-home.component.scss']
})
export class BlogHomeComponent implements OnInit {

  loading$: Observable<boolean> = of(true);
  lastpost$: Observable<PostList[]> = of([]);

  constructor(private store: Store<AppStateBlog>) { }

  ngOnInit(): void {
    this.loading$ = this.store.select(selectors.GetLoadingPost);
    this.lastpost$ = this.store.select(selectors.GetLastPosts);
    this.store.dispatch(actions.LoadingLastPost({size: 5}));
  }

}
