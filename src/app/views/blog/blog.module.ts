import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BlogRoutingModule } from './blog.routing';
import { SharedModule } from '../../shared/shared.module';
import * as reducers from '../../../redux/reducers';
import { AppState } from '../../../redux/app.reducers';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { PostEffects } from '../../../redux/effects/post.effects';
import { ComponentsModule } from '../../components/components.module';
import { BlogListComponent } from './blog-list/blog-list.component';
import { BlogDetailComponent } from './blog-detail/blog-detail.component';
import { BlogHomeComponent } from './blog-home/blog-home.component';
import { PipesModule } from '../../pipes/pipes.module';


const blogreducers = {
  post: reducers.postReducer
}

export interface AppStateBlog extends AppState {
  post: reducers.PostState;
}


@NgModule({
  declarations: [BlogListComponent, BlogDetailComponent, BlogHomeComponent],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ComponentsModule,
    ReactiveFormsModule,
    BlogRoutingModule,
    SharedModule,
    PipesModule,
    StoreModule.forFeature('blog', {post: reducers.postReducer}),
    EffectsModule.forFeature([PostEffects])
  ]
})
export class BlogModule { }
