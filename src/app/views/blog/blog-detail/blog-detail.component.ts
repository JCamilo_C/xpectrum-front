import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { Post } from '../../../../models/blog/post';
import { AppStateBlog } from '../blog.module';
import * as selectors from '../../../../redux/selectors';
import * as actions from '../../../../redux/actions';
import { Location } from '@angular/common';

@Component({
  selector: 'app-blog-detail',
  templateUrl: './blog-detail.component.html',
  styleUrls: ['./blog-detail.component.scss']
})
export class BlogDetailComponent implements OnInit {
  loading$: Observable<boolean> = of(false);
  postDetail$: Observable<Post> = of(null);
  idPost: number;

  constructor(private activateRoutes: ActivatedRoute, private store: Store<AppStateBlog>, private location: Location) {
    this.idPost = Number(this.activateRoutes.snapshot.paramMap.get('id'));
  }

  ngOnInit(): void {
    this.loading$ = this.store.select(selectors.GetLoadingPost);
    this.postDetail$ = this.store.select(selectors.GetPostDetail);

    this.store.dispatch(actions.LoadingPostDetail({ id: this.idPost }))
  }

  back() {
    this.location.back()
  }

}
