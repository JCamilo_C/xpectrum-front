import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { AppState } from 'src/redux/app.reducers';
import { Menu } from '../../../models/ui/menu';
import * as selectors from '../../../redux/selectors';
import * as actions from '../../../redux/actions';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent implements OnInit {
  menu_teacher: Menu[] = [
    {
      icon: 'fas fa-globe-americas',
      name: 'Inicio',
      url: '/Dashboard'
    },
    {
      icon: 'far fa-newspaper',
      name: 'Post',
      url: '/List'
    }
  ] 

  showToolbar$: Observable<boolean> = of(true)

  constructor(private store: Store<AppState>) {
    this.showToolbar$ = this.store.select(selectors.GetSidebarStatus);
  }

  ngOnInit(): void {
  }

}
