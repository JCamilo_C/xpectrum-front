import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { PostList } from 'src/models/blog/post';
import { AppStateBlog } from '../blog.module';
import * as actions from '../../../../redux/actions';
import * as selectors from '../../../../redux/selectors';
import { Router } from '@angular/router';
import { Paginate } from '../../../../models/response/response.model';

@Component({
  selector: 'app-blog-list',
  templateUrl: './blog-list.component.html',
  styleUrls: ['./blog-list.component.scss']
})
export class BlogListComponent implements OnInit {
  loading$: Observable<boolean> = of(true);
  posts$: Observable<Paginate<PostList[]>> = of(null);
  page: number = 1;

  constructor(private store: Store<AppStateBlog>, private router: Router) { }

  ngOnInit(): void {
    this.loading$ = this.store.select(selectors.GetLoadingPost);
    this.posts$ = this.store.select(selectors.GetAllPosts);

    this.store.dispatch(actions.LoadingAllPost({ page: this.page }));
  }

  redirect(id: number) {
    this.router.navigate(['/Detail', id]);
  }

  changePage(page: number) {
    this.page = page;
    this.store.dispatch(actions.LoadingAllPost({ page: this.page }));
  }

}
