import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateAgo'
})
export class DateAgoPipe implements PipeTransform {

  transform(value: Date): string {
    var today: Date = new Date();
    var diffMs = Math.abs(value.getTime() - today.getTime()); // milliseconds between now & Christmas
    var diffDays = Math.floor(diffMs / 86400000); // days
    var diffHrs = Math.floor((diffMs % 86400000) / 3600000); // hours
    var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
    if (diffDays > 0) {
      return `${diffDays} día(s)`;
    } else if (diffHrs > 0) {
      return `${diffHrs} hora(s)`;
    } else if (diffMins > 0) {
      return `${diffMins} minuto(s)`;
    } else {
      return `pocos segundos`;
    }
  }

}
