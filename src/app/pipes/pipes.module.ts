import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ParseDatePipe } from './parse-date.pipe';
import { DateAgoPipe } from './date-ago.pipe';
import { SafeHtmlPipe } from './safe-html.pipe';



@NgModule({
  declarations: [ParseDatePipe, DateAgoPipe, SafeHtmlPipe],
  imports: [
    CommonModule
  ],
  exports: [ParseDatePipe, DateAgoPipe, SafeHtmlPipe]
})
export class PipesModule { }
