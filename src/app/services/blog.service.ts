import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Paginate, ResponseModel } from 'src/models/response/response.model';
import { environment } from '../../environments/environment';
import { PostList, Post, CreatePost } from '../../models/blog/post';

@Injectable({
  providedIn: 'root'
})
export class BlogService {

  private endpoint = `${environment.backUrl}/post`;

  constructor(private http: HttpClient) { }

  getAllPost(page: number): Observable<ResponseModel<Paginate<PostList[]>>> {
    return this.http.get<ResponseModel<Paginate<PostList[]>>>(`${this.endpoint}/all?page=${page}`);
  }

  getLastPosts(size: number): Observable<ResponseModel<PostList[]>> {
    return this.http.get<ResponseModel<PostList[]>>(`${this.endpoint}/last/${size}`);
  }

  getPostDetail(id: number): Observable<ResponseModel<Post>> {
    return this.http.get<ResponseModel<Post>>(`${this.endpoint}/detail/${id}`);
  }

  createPost(post: CreatePost): Observable<ResponseModel<PostList>> {
    return this.http.post<ResponseModel<PostList>>(`${this.endpoint}/create`, post);
  }

  updatePost(post: Post): Observable<ResponseModel<null>> {
    return this.http.put<ResponseModel<null>>(`${this.endpoint}/update`, post);
  }
}
