import { TestBed } from '@angular/core/testing';

import { ErrorTokenService } from './error-token.service';

describe('ErrorTokenService', () => {
  let service: ErrorTokenService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ErrorTokenService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
