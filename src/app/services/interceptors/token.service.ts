import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { mergeMap, take, switchMap, catchError } from 'rxjs/operators';
import { AppState } from '../../../redux/app.reducers';
import * as selectors from '../../../redux/selectors';
import { Authentication } from '../../../models/auth/authentication.model';


@Injectable({
  providedIn: 'root'
})
export class TokenService implements HttpInterceptor{

  constructor(private store: Store<AppState>) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const skip = req.headers.get('skip');
    if (!skip) {
      return this.store.select(selectors.GetToken).pipe(
        mergeMap( token => {
          
          if (!skip) {
            if (token === '') {
              return next.handle(req);
            }
            const headers = req.clone({
              headers: req.headers.set('x-token', token)
            });
            return next.handle(headers);
          }
          return next.handle(req);
        })
      )
    }

    return next.handle(req);
    
  }
}
