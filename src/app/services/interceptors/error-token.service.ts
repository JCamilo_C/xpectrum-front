import { HttpInterceptor, HttpRequest, HttpEvent, HttpHandler } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AppState } from 'src/redux/app.reducers';
import { LogOutAction } from '../../../redux/actions/auth.actions'


@Injectable({
  providedIn: 'root'
})
export class ErrorTokenService implements HttpInterceptor {

  constructor(private store: Store<AppState>) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(catchError(err => {
        if ([401, 403].includes(err.status)) {
            // auto logout if 401 or 403 response returned from api
            this.store.dispatch(LogOutAction())
        }

        const error = (err && err.error && err.error.message) || err.statusText;
        return throwError(err);
    }));
  }
}
