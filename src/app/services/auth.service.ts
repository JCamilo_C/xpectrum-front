import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { ResponseModel } from '../../models/response/response.model';
import { Authentication } from '../../models/auth/authentication.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private endpoint = `${environment.backUrl}/auth`;

  constructor(private http: HttpClient) { }

  Login(credenciales: {email: string, password: string}): Observable<ResponseModel<Authentication>> {
    return this.http.post<ResponseModel<Authentication>>(`${this.endpoint}/login`, credenciales, {headers: {skip: 'true'}});
  }
}
