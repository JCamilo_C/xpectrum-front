import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../../redux/app.reducers';
import * as actions from '../../../redux/actions';
import * as selectors from '../../../redux/selectors';
import { Observable, of } from 'rxjs';
import { User } from 'src/models/auth/user.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  private show: boolean = true;
  user: Observable<User> = of(null);

  constructor(private store: Store<AppState>) { }

  ngOnInit(): void {
    this.user = this.store.select(selectors.GetUser);
  }

  ToogleMenu() {
    this.store.dispatch(actions.toogleToolbarAction({ payload: !this.show }));
    this.show = !this.show;
  }

}
