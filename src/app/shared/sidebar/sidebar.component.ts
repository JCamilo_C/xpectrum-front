import { Component, Input, OnInit } from '@angular/core';
import { Menu } from '../../../models/ui/menu';
import { Store } from '@ngrx/store';
import { AppState } from 'src/redux/app.reducers';
import * as actions from '../../../redux/actions'
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  @Input() menu: Menu[] = [];

  constructor(private store: Store<AppState>, private router: Router) { }

  ngOnInit(): void {
  }

  logOut() {
    this.store.dispatch(actions.LogOutAction());
  }
}
