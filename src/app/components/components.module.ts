import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CarouselComponent } from './carousel/carousel.component';
import { PipesModule } from '../pipes/pipes.module';
import { RouterModule } from '@angular/router';
import { ListPostComponent } from './list-post/list-post.component';
import { PaginatorComponent } from './paginator/paginator.component';
import { ModalCreatePostComponent } from './modal-create-post/modal-create-post.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [CarouselComponent, ListPostComponent, PaginatorComponent, ModalCreatePostComponent],
  imports: [
    CommonModule,
    PipesModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    PipesModule
  ],
  exports: [CarouselComponent, ListPostComponent, PaginatorComponent, ModalCreatePostComponent]
})
export class ComponentsModule { }
