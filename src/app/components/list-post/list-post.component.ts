import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PostList } from '../../../models/blog/post';

@Component({
  selector: 'app-list-post',
  templateUrl: './list-post.component.html',
  styleUrls: ['./list-post.component.scss']
})
export class ListPostComponent implements OnInit {

  @Input() posts: PostList[] = [];
  @Output() selected: EventEmitter<number> = new EventEmitter<number>(null);

  constructor() { }

  ngOnInit(): void {
  }

  OnClick(id: number, e) {
    e.preventDefault();
    this.selected.emit(id);
  }

}
