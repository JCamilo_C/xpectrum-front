import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss']
})
export class PaginatorComponent implements OnInit {

  @Input() pages: number = 0; 
  @Input() page: number = 1; 
  @Input() total: number = 0;
  @Output() eventPage: EventEmitter<number> = new EventEmitter(null);

  constructor() { }

  ngOnInit(): void {
  }

  changePage(page:number, e) {
    e.preventDefault();
    this.page = page;
    this.eventPage.emit(this.page);
  }

  createArray(range: number) {
    return new Array(range);
  }

}
