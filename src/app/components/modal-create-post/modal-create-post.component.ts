import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppStateBlog } from '../../views/blog/blog.module';
import { CreatePost, Post } from '../../../models/blog/post';
import * as actions from '../../../redux/actions';
import * as selectors from '../../../redux/selectors';
import { take } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'app-modal-create-post',
  templateUrl: './modal-create-post.component.html',
  styleUrls: ['./modal-create-post.component.scss']
})
export class ModalCreatePostComponent implements OnInit {
  @Input() post: Post = null;
  loading$: Observable<boolean> = of(false);
  form: FormGroup;
  errors: string[] = [];
  nameAction = 'Crear';

  constructor(private store: Store<AppStateBlog>) { 
    this.form = new FormGroup({
      title: new FormControl('', [Validators.required]),
      short_description: new FormControl('', [Validators.required, Validators.maxLength(75)]),
      description: new FormControl('', [Validators.required]),
      image: new FormControl('', [Validators.pattern(/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/)]),
      autor: new FormControl(''),
    })
  }

  ngOnInit(): void {
    this.loading$ = this.store.select(selectors.GetLoadingPost);
    if (this.post) {
      this.nameAction = 'Actualizar'
      this.form.setValue({
        title: this.post.title,
        short_description: this.post.short_description,
        description: this.post.description,
        image: this.post.image,
        autor: this.post.autor
      })
    }
  }

  onSubmit() {
    if (this.form.valid) {
      this.store.select(selectors.GetUser).pipe(take(1)).subscribe( u => {
        if (this.post) {
          const updatePost: Post = {...this.post, ...this.form.value};
          updatePost.description = updatePost.description.replace(/\.(\r\n|\r|\n)/g, '.<br /><br />');
          this.store.dispatch(actions.LoadingUpdatePost({ post: updatePost}))
        } else {
          const newPost: CreatePost = {...this.form.value, autor: u.name};
          if (newPost.image == '') newPost.image = 'https://i.pinimg.com/originals/e7/d4/a2/e7d4a2821550e5651644dc05feccb1a9.jpg'
          newPost.description = newPost.description.replace(/\.(\r\n|\r|\n)/g, '.<br /><br />');
          this.store.dispatch(actions.LoadingCreatePost({ post: newPost}))
        }
      });
    }
  }

}
