import { Component, OnInit, Input } from '@angular/core';
import { PostList } from '../../../models/blog/post';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent implements OnInit {

  @Input() posts: PostList[] = []

  constructor() { }

  ngOnInit(): void {
  }

}
