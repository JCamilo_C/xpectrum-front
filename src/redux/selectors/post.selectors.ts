import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AppStateBlog } from '../../app/views/blog/blog.module';

const SelectPostState = createFeatureSelector<AppStateBlog>('blog');

export const GetLoadingPost = createSelector(
    SelectPostState,
    (state: AppStateBlog) => state.post.loading
);

export const GetAllPosts = createSelector(
    SelectPostState,
    (state: AppStateBlog) => state.post.posts
);

export const GetLastPosts = createSelector(
    SelectPostState,
    (state: AppStateBlog) => state.post.lastPosts
);

export const GetPostDetail = createSelector(
    SelectPostState,
    (state: AppStateBlog) => state.post.postDetail
)
