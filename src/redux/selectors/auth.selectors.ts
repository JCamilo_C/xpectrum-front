import { createSelector } from '@ngrx/store';
import { AppState } from '../app.reducers';
import { AuthState } from '../reducers/auth.reducer';

const SelectAuth = (state: AppState) => state.auth;

export const IsAuth = createSelector(
    SelectAuth,
    (state: AuthState) => state.auth !== null
);

export const GetAuthLoading = createSelector(
    SelectAuth,
    (state: AuthState) => state.loading
);

export const GetUser = createSelector(
    SelectAuth,
    (state: AuthState) => state.auth !== null ? state.auth.user : null
);

export const GetToken = createSelector(
    SelectAuth,
    (state: AuthState) => state.auth !== null ? state.auth.token : null
);
