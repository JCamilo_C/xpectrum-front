import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { BlogService } from '../../app/services/blog.service';
import * as fromactions from '../actions'
import { switchMap, catchError, tap, mergeMap } from 'rxjs/operators';
import { TYPE_WARNING, TYPE_ERROR, TYPE_SUCCESS } from '../../models/constants';
import { HttpErrorResponse } from '@angular/common/http';
import { HttpHandler } from '../../helpers/http-handlers';
import { of } from 'rxjs';

@Injectable()
export class PostEffects {

    constructor(private actions: Actions, private blogService: BlogService) {}

    loadingLastPosts$ = createEffect(() => this.actions.pipe(
        ofType(fromactions.LoadingLastPost),
        mergeMap( (action) => this.blogService.getLastPosts(action.size).pipe(
            mergeMap( (response) => {
                if (response.Response) {
                    if (response.Result) return [fromactions.LastPost({posts: response.Result })]
                    else return [
                        fromactions.toastMessageAction({message: response.Message, typeNotification: TYPE_WARNING})
                    ]
                } else {
                    return [
                        fromactions.toastMessageAction({message: response.Message, typeNotification: TYPE_WARNING})
                    ]
                }
            }),
            catchError( (error: HttpErrorResponse | any) => {
                if (error?.status != null) {
                    const handler = new HttpHandler(error);
                    return of(
                        fromactions.toastMessageAction({message: handler.getHttpError(), typeNotification: TYPE_ERROR})
                    )
                }
                return of(fromactions.LoginFailAction({payload: error.message}))
            })
        ))
    ));
    
    loadingPostDetail$ = createEffect(() => this.actions.pipe(
        ofType(fromactions.LoadingPostDetail),
        switchMap( (action) => this.blogService.getPostDetail(action.id).pipe(
            switchMap( (response) => {
                if (response.Response) {
                    if (response.Result) return [fromactions.PostDetail({post: response.Result })]
                    else return [
                        fromactions.toastMessageAction({message: response.Message, typeNotification: TYPE_WARNING})
                    ]
                } else {
                    return [
                        fromactions.toastMessageAction({message: response.Message, typeNotification: TYPE_WARNING})
                    ]
                }
            }),
            catchError( (error: HttpErrorResponse) => {
                const handler = new HttpHandler(error);
                return [
                    fromactions.toastMessageAction({message: handler.getHttpError(), typeNotification: TYPE_ERROR})
                ]
            })
        ))
    ));
    
    loadingAllPost$ = createEffect(() => this.actions.pipe(
        ofType(fromactions.LoadingAllPost),
        switchMap( (action) => this.blogService.getAllPost(action.page).pipe(
            switchMap( (response) => {
                if (response.Response) {
                    if (response.Result) return [fromactions.AllPost({posts: response.Result })]
                    else return [
                        fromactions.toastMessageAction({message: response.Message, typeNotification: TYPE_WARNING})
                    ]
                } else {
                    return [
                        fromactions.toastMessageAction({message: response.Message, typeNotification: TYPE_WARNING})
                    ]
                }
            }),
            catchError( (error: HttpErrorResponse) => {
                const handler = new HttpHandler(error);
                return [
                    fromactions.toastMessageAction({message: handler.getHttpError(), typeNotification: TYPE_ERROR})
                ]
            })
        ))
    )); 

    loadingCreatePost$ = createEffect(() => this.actions.pipe(
        ofType(fromactions.LoadingCreatePost),
        switchMap( (action) => this.blogService.createPost(action.post).pipe(
            switchMap( (response) => {
                if (response.Response) {
                    if (response.Result) return [
                        fromactions.toastMessageAction({message: response.Message, typeNotification: TYPE_SUCCESS}),
                        fromactions.CreatePostAction({post: response.Result })
                    ]
                    else return [
                        fromactions.toastMessageAction({message: response.Message, typeNotification: TYPE_WARNING})
                    ]
                } else {
                    return [
                        fromactions.toastMessageAction({message: response.Message, typeNotification: TYPE_WARNING})
                    ]
                }
            }),
            catchError( (error: HttpErrorResponse) => {
                const handler = new HttpHandler(error);
                return [
                    fromactions.toastMessageAction({message: handler.getHttpError(), typeNotification: TYPE_ERROR})
                ]
            })
        ))
    ));

    loadingUpdatePost$ = createEffect(() => this.actions.pipe(
        ofType(fromactions.LoadingUpdatePost),
        switchMap( (action) => this.blogService.updatePost(action.post).pipe(
            switchMap( (response) => {
                if (response.Response) {
                    return [
                        fromactions.toastMessageAction({message: response.Message, typeNotification: TYPE_SUCCESS}),
                        fromactions.UpdatePostAction({post: action.post })
                    ]
                } else {
                    return [
                        fromactions.toastMessageAction({message: response.Message, typeNotification: TYPE_WARNING})
                    ]
                }
            }),
            catchError( (error: HttpErrorResponse) => {
                const handler = new HttpHandler(error);
                return [
                    fromactions.toastMessageAction({message: handler.getHttpError(), typeNotification: TYPE_ERROR})
                ]
            })
        ))
    ));

}