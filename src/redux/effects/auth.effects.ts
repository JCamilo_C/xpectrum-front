import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { AuthService } from '../../app/services/auth.service';
import { catchError, switchMap, tap, mergeMap, map } from 'rxjs/operators';
import * as fromactions from '../actions'
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { HttpHandler } from '../../helpers/http-handlers';
import { TYPE_WARNING, TYPE_ERROR } from '../../models/constants';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class AuthEffects {

    constructor(private actions: Actions,private authService: AuthService, private router: Router, private toastr: ToastrService) {}

    LoadingLogin$ = createEffect( () => this.actions.pipe(
        ofType(fromactions.LoadingLoginAction),
        mergeMap( (action) => this.authService.Login({email: action.email, password: action.password}).pipe(
            mergeMap( (response) => {
                if (response.Response) {
                    if (response.Result) {
                        return [fromactions.LoginSuccessAction({payload: response.Result})]
                    }
                    else{ return [
                        fromactions.LoginFailAction({payload: response.Message}),
                        fromactions.toastMessageAction({message: response.Message, typeNotification: TYPE_WARNING})
                    ]}
                } else {
                    return [
                        fromactions.LoginFailAction({payload: response.Message}),
                        fromactions.toastMessageAction({message: response.Message, typeNotification: TYPE_WARNING})
                    ]
                }
            }),
            catchError( (error: HttpErrorResponse) => {
                const handler = new HttpHandler(error);
                return [
                    fromactions.LoginFailAction({payload: handler.getHttpError()}),
                    fromactions.toastMessageAction({message: handler.getHttpError(), typeNotification: TYPE_ERROR})
                ]
            })
        ))
    ));


    LogInAction$ = createEffect( () => this.actions.pipe(
        ofType(fromactions.LoginSuccessAction),
        tap( () => this.router.navigateByUrl('/Dashboard'))
    ), {dispatch: false});

    LogOut$ = createEffect( () => this.actions.pipe(
        ofType(fromactions.LogOutAction),
        tap( () => this.router.navigateByUrl('/Auth/Login'))
    ), {dispatch: false});
}