import { ActionReducer, ActionReducerMap, MetaReducer } from "@ngrx/store";
import { AuthState } from "./reducers/auth.reducer";
import * as reducers from './reducers'
import { localStorageSync } from 'ngrx-store-localstorage';
import { UiState } from './reducers/ui.reducers';

export interface AppState {
    auth: AuthState;
    ui: UiState;
}

export const appReducers: ActionReducerMap<AppState> = {
    auth: reducers.authReducer,
    ui: reducers.uiReducer
}

export function localStorageSyncReducer(reducer: ActionReducer<any>): ActionReducer<any> {
    return localStorageSync({
        keys: ['auth'],
        rehydrate: true
    })(reducer);
}

export const metaReducers: Array<MetaReducer<any, any>> = [localStorageSyncReducer];