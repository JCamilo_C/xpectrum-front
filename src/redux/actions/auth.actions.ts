import { createAction, props } from '@ngrx/store'
import { Authentication } from '../../models/auth/authentication.model';

export const LoadingLoginAction = createAction(
    '[AUTH] Loading login ....',
    props<{ email: string, password: string }>()
);


export const LoginSuccessAction = createAction(
    '[AUTH] Login success ....',
    props<{ payload: Authentication }>()
);

export const LoginFailAction = createAction(
    '[AUTH] Login fail ....',
    props<{ payload: string }>()
);

export const LogOutAction = createAction(
    '[AUTH] Logout ....'
);