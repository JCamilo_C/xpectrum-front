import { createAction, props } from '@ngrx/store';
import { Paginate } from 'src/models/response/response.model';
import { PostList, Post, CreatePost } from '../../models/blog/post';

export const LoadingAllPost = createAction(
    '[BLOG] Loading all posts',
    props<{ page: number}>()
);

export const AllPost = createAction(
    '[BLOG] All posts',
    props<{ posts: Paginate<PostList[]>}>()
);

export const LoadingLastPost = createAction(
    '[BLOG] Loading last posts',
    props<{ size: number}>()
);

export const LastPost = createAction(
    '[BLOG] Last posts',
    props<{ posts: PostList[]}>()
);

export const LoadingPostDetail = createAction(
    '[BLOG] Loading post detail',
    props<{ id: number}>()
);

export const PostDetail = createAction(
    '[BLOG] Post detail',
    props<{ post: Post }>()
);

export const LoadingCreatePost = createAction(
    '[BLOG] Loading create post',
    props<{ post: CreatePost}>()
);

export const CreatePostAction = createAction(
    '[BLOG] Create post',
    props<{ post: PostList}>()
);

export const LoadingUpdatePost = createAction(
    '[BLOG] Loading update post',
    props<{ post: Post}>()
);

export const UpdatePostAction = createAction(
    '[BLOG] Update post',
    props<{ post: Post}>()
);