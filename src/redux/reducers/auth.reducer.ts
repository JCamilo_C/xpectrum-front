import { createReducer, on, Action } from '@ngrx/store';
import { Authentication } from '../../models/auth/authentication.model';
import * as authActions from '../actions/auth.actions';

export interface AuthState {
    loading: boolean;
    auth: Authentication;
    error: string;
}

const initState: AuthState = {
    loading: false,
    auth: null,
    error: null
}

const Reducer = createReducer(
    initState,
    on(authActions.LoadingLoginAction, (state) => ({ ...state, loading: true })),
    on(authActions.LoginSuccessAction, (state, {payload}) => ({ ...state, loading: false, auth: {...payload} })),
    on(authActions.LogOutAction, (state) => ({...initState })),
    on(authActions.LoginFailAction, (state, {payload}) => ({...state, loading: false, auth: null, error: payload })),
)

export function authReducer(state = initState, action: Action): AuthState {
    return Reducer(state, action);
}