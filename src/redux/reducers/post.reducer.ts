import { Post, PostList, CreatePost } from '../../models/blog/post';
import { Action, createReducer, on } from '@ngrx/store';
import * as actions from '../actions/post.actions';
import { Paginate } from 'src/models/response/response.model';

export interface PostState {
    loading: boolean;
    posts: Paginate<PostList[]>;
    lastPosts: PostList[];
    postDetail: Post;
    error: string;
}

const initState: PostState = {
    loading: false,
    posts: null,
    lastPosts: [],
    postDetail: null,
    error: null
}


const Reducer = createReducer(
    initState,
    on(
        actions.LoadingLastPost,
        actions.LoadingPostDetail,
        actions.LoadingAllPost,
        actions.LoadingCreatePost,
        actions.LoadingUpdatePost,
        (state) => ({ ...state, loading: true })),
    on(actions.LastPost, (state, {posts}) => ({ ...state, loading: false, lastPosts: [...posts] })),
    on(actions.PostDetail, (state, {post}) => ({ ...state, loading: false, postDetail: {...post} })),
    on(actions.AllPost, (state, {posts}) => ({ ...state, loading: false, posts: {...posts} })),
    on(actions.CreatePostAction, (state, {post}) => { 
        let tmp_copy_items: PostList[] = [post];
        if (state.posts !== null) {
            if (state.posts?.items.length > 0) {
                const slice = state.posts.items.slice(1, state.posts.items.length)
                tmp_copy_items = [...slice];
            }
        }
        return {...state, loading: false, posts: { ...state.posts, items: [...tmp_copy_items]}}}
    ),
    on(actions.UpdatePostAction, (state, {post}) => ({...state, loading: false, postDetail: {...post}}))
);

export function postReducer(state = initState, action: Action): PostState {
    return Reducer(state, action)
}