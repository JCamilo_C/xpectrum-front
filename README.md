# Front

Este proyecto es generado con [Angular CLI](https://github.com/angular/angular-cli) version 10.0.5.

# Dependencias 

- NgRx (10.0.0): Paquete para manejar estados
- Bootstrap (4.5.2): Paquete para framework css
- ngrx-store-localstorage" (4.5.2): Paquete de sincronización de estados con local storage
- NgRx Effects (4.5.2): Paquete para realizar acciones sobre estados

# Resumen
Este proyecto trabaja bajo una arquitectura tradicional de componentes, vistas y servicios. Ademas de las herramientas como lo son los pipes, guards, helpers entre otros.

### Explicación de la arquitectura
- componentes: son pequeñas secciones de codigo pensado para ser reutilizados por las vistas
- guards: Es una herramienta para proteger las rutas, en este caso el dashboard
- pipes: Son funciones de extension que se usa para dar formatos o acceder a informacion asincrona
- services: Seccion de codigo que permite comunicacion con API's en este caso el backend
- shared: Son secciones especiales que forman el layout a diferencia de los componentes estan pensados como gestion unicamente del layout
- vistas: Son los componentes que sirve como plantillas o layouts
- redux: Contiene todo el manejador de estados
  - actions: Acciones disponble para ejecutar en los estados
  - effects: Listeners que ejecutan acciones a partir de otra accion
  - reducers: Gestiona los diferentes states

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

